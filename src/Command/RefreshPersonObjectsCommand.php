<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use \Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:refresh-person-objects',
    description: 'Save all objects to trigger onSave events',
    hidden: false,
    aliases: ['app:refresh-person-objects']
)]
class RefreshPersonObjectsCommand extends Command {
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $people = new DataObject\Person\Listing();

        foreach($people as $person) {
            $person->save();
            $output->writeln(
                sprintf(
                    'Person (id: %s, full_name: %s %s) has been updated',
                    $person->getId(),
                    $person->getName(),
                    $person->getLastName()
                )
            );
        }

        return Command::SUCCESS;
    }
}
