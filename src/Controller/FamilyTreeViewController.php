<?php

namespace App\Controller;

use Carbon\Carbon;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Pimcore\Model\DataObject;

class FamilyTreeViewController extends FrontendController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        return $this->render('family-tree-view/index.html.twig', [
            'nodes' => $this->buildNodes()
        ]);
    }

    private function buildNodes(): array
    {
        $people = new DataObject\Person\Listing();
        // $people->setUnpublished(true);

        $nodeList = [];

        foreach($people as $person){
            $parners = $person->getPartners();
            $photo = $person->getPhoto()?->getThumbnail()->getPath();

            $stringBirthDate = $person->getBirthDate();
            $birthDate = new Carbon($stringBirthDate);
            // Theres a bug with parsing date and it gives date with minus 1 day, so I add one to compensate.
            $birthDate->addDay();

            $nodeList[] = [
                'id' => $person->getId(),
                'pids' => !empty($parners) ? array_map(function($parner){
                    return $parner->getId();
                }, $parners) : null,
                'mid' => $person->getDad()?->getId(),
                'fid' => $person->getMom()?->getId(),
                'name' => $person->getName() . ' ' . $person->getLastName(),
                'gender' => $person->getSex(),
                'photo' => !empty($photo) ? stripcslashes($photo) : null,
                'born' => !empty($stringBirthDate) ? $birthDate->format('d/m/Y') : null
            ];
        }

        return $nodeList;
    }
}
