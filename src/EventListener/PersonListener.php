<?php

declare(strict_types=1);

namespace App\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Person;
use Psr\Log\LoggerInterface;

class PersonListener
{
    public function __construct(
        private LoggerInterface $logger
    ) {

    }

    public function setNameAndLastName(DataObjectEvent $event): void
    {
        $object = $event->getObject();

        if ($object instanceof Person) {
            $fullName = $object->getKey();
            $arrayNames = explode(' ', $fullName);

            if (isset($arrayNames[0])) {
                if(empty($object->getName())){
                    $object->setName($arrayNames[0]);
                }
            }


            if (isset($arrayNames[1])) {
                if(empty($object->getLastName())){
                    $object->setLastName($arrayNames[1]);
                }
            }
        }
    }

    public function setRelatedPartners(DataObjectEvent $event): void
    {
        $object = $event->getObject();

        if ($object instanceof Person) {
            /** @var Person[] partners */
            $partners = $object->getPartners();

            foreach($partners as $partner){
                $partnerPartners = $partner->getPartners();

                $partner->setPartners(
                    array_unique(
                        array_merge($partnerPartners, [$object])
                    )
                );

                $partner->save();
            }
        }
    }
}
